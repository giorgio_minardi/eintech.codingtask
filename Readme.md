﻿# Readme

Eintech CodingTask.

## Overview 
  - Simple Vue.js form, without using templates/npm packages, the vue.js script is included via CDN.
  - No repository or service layers since there we are doing simple CRUD operations.
  - The override of OnBeforeSaving on the DbContext is responsible for setting the created date of the Person record.
  - Using PersonViewModel to pass the data from the form to the REST api. Mapping to the EF entity is done within the controller for the sake of simplicity. 
  In a larger project extension methods or Automapper should be used.
  - FluentValidation behind the scene. While we are performing a lightweight clientside validation on the form using Vue, an implicit ModelState validation is perfomed behind the scene when
  posting the payload to the webapi. This can be tried via Postman or using the integration test attached: a 400 error is returned if the Person name is missing.
  - Using SQLlite as database for simplicity. Db has been created using the EF add-database command.
  - Using in memory database for both unit testing and integration testing.
  - A GET and DELETE method have been added too just to show within the UI the records been added. Since they weren't part of the specs (and the lack of time) the methods have no test coverage :) Only the POST has.
  - Using async/await just for the sake of it even if they're not necessary in this case.

## Conclusion

Thought of using JQuery and rely on unobstrusive validation but ended up going with Vue since it is quite popular nowdays. This is not a SPA intentionally since to run a SPA npm is needed and i tried to keep it simple.
I would have liked to try AlpineJs of Htmx.js but didn't have much time. In fact I apologise for the delay: I'm currently in Rome at my parents place and didn't have much free time until now!

