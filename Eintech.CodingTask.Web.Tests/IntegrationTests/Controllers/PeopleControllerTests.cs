﻿using Eintech.CodingTask.Web.Data.EF;
using Eintech.CodingTask.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xunit;

namespace Eintech.CodingTask.Web.Tests.IntegrationTests.Controllers
{
    public class PeopleControllerTests : IClassFixture<CustomWebApplicationFactory<Eintech.CodingTask.Web.Startup>>
    {
        private readonly CustomWebApplicationFactory<Eintech.CodingTask.Web.Startup> _factory;

        public PeopleControllerTests(CustomWebApplicationFactory<Eintech.CodingTask.Web.Startup> factory)
        {
            _factory = factory;
        }

        // using an integration test to leverage model validation provided by fluentvalidation.
        [Theory]
        [InlineData("John", 200)]
        [InlineData("", 400)]
        [InlineData(null, 400)]
        public async Task Post_Invalid_Person_Model(string name, int statusCode)
        {
            // Arrange
            // creating an person model
            var personModel = new PersonViewModel() { Name = name };
            var client = _factory.CreateClient();

            // Act
            var dataAsString = JsonConvert.SerializeObject(personModel);
            var content = new StringContent(dataAsString);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await client.PostAsync("api/people", content);

            // Assert
            Assert.Equal(statusCode, (int)response.StatusCode);

        }
    }


    // Custom webhost using in memory database
    public class CustomWebApplicationFactory<TStartup>
    : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                        typeof(DbContextOptions<CodingTaskDataContext>));

                services.Remove(descriptor);

                services.AddDbContext<CodingTaskDataContext>(options =>
                {
                    options.UseInMemoryDatabase("CodingTaskMemDatabase");
                });

                // Build the service provider.
                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<CodingTaskDataContext>();

                    db.Database.EnsureCreated();
                }
            });
        }
    }
}
