﻿using Eintech.CodingTask.Web.Controllers;
using Eintech.CodingTask.Web.Data.EF;
using Eintech.CodingTask.Web.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Eintech.CodingTask.Web.Tests.UnitTests.Controllers
{
    public class PeopleControllerTests
    {
        private readonly PeopleController _peopleController;
        private readonly ICodingTaskDataContext _dataContext;

        public PeopleControllerTests()
        {
            var options = new DbContextOptionsBuilder<CodingTaskDataContext>()
               .UseInMemoryDatabase(databaseName: "CodingTaskMemDatabase")
               .Options;
            _dataContext = new CodingTaskDataContext(options);
            _peopleController = new PeopleController(_dataContext);
            ((CodingTaskDataContext)_dataContext).Database.EnsureCreated();
        }



        [Theory]
        [InlineData("John", true)]
        public async Task Post_Person_Model_Ensure_Is_Added_To_Db(string name, bool success)
        {
            // Arrange
            // creating an person model
            var personModel = new PersonViewModel() { Name = name };

            // Act
            // posting the model to the client
            await _peopleController.Post(personModel);

            // Assert
            // verify if an entry has been added in the db
            var result = _dataContext.Persons.Any(x => x.Name == name);
            Assert.True(result == success);
        }

        [InlineData("John", true)]
        [InlineData("", false)]
        [InlineData(null, false)]
        public async Task Post_Valid_Invsalid_Person(string name, bool success)
        {

        }

    }
}
