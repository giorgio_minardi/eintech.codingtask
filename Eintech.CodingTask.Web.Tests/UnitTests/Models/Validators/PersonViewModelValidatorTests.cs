﻿using Eintech.CodingTask.Web.Models.Validators;
using FluentValidation.TestHelper;

using Xunit;

namespace Eintech.CodingTask.Web.Tests.UnitTests.Models.Validators
{
    public class PersonViewModelValidatorTests
    {
        private PersonViewModelValidator _validator;

        public PersonViewModelValidatorTests()
        {
            _validator = new PersonViewModelValidator();
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Should_Have_Error_When_Name_Is_Null_Or_Empty(string name)
        {
            _validator.ShouldHaveValidationErrorFor(person => person.Name, name);
        }

        [Fact]
        public void Should_Not_Have_Error_When_Name_Has_Value()
        {
            _validator.ShouldNotHaveValidationErrorFor(person => person.Name, "John");
        }
    }
}
