﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

const app = new Vue({
    el: '#app',
    data: {
        errors: [],
        person: {
            name: null,
            created: null
        },
        people: null
    },
    

    methods: {
        getPeople: function () {
            axios.get('api/people')
                .then(response => {
                    debugger;
                    this.people = response.data;
                })
                .catch(err => {
                    console.log(err);
                });
        },
        checkForm: function (e) {
            this.errors = [];
            debugger;
            e.preventDefault();
            if (!this.person.name) {
                this.errors.push("Name required.");
                return false;
            }          
            axios.post('api/people', JSON.stringify(this.person), {
                headers: {
                    'Content-Type': 'application/json'
                }
            })      
            .then(response => {
                this.getPeople();
            })
            .catch(err => {
                console.log(err);
            });
        },
        deletePerson: function (id) {
            axios.delete('api/people/'+id)
                .then(response => {
                    this.getPeople();
                })
                .catch(err => {
                    console.log(err);
                });
        },
        formatDate: function (date) {
            return new Date(date).toISOString().split('T')[0];
        }
    }
})
