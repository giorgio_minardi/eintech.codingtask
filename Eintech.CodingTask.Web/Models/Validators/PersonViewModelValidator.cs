﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eintech.CodingTask.Web.Models.Validators
{
    public class PersonViewModelValidator : AbstractValidator<PersonViewModel>
    {
        public PersonViewModelValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Please specify a first name");
        }
    }
}
