﻿using Eintech.CodingTask.Web.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Eintech.CodingTask.Web.Data.EF
{

    public class CodingTaskDataContext : DbContext, ICodingTaskDataContext
    {
        public DbSet<Person> Persons { get; set; }

        public CodingTaskDataContext(DbContextOptions options) : base(options) {}

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void OnBeforeSaving()
        {
            foreach (var entry in ChangeTracker.Entries<AuditedEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.Created = DateTime.Now;
                        entry.Entity.Modified = DateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.Modified = DateTime.Now;
                        break;
                }
            }
        }
    }
}
