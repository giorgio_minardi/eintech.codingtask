﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Eintech.CodingTask.Web.Data.Entities
{ 
    public class Person : AuditedEntity
    {
        public string Name { get; set; }
    }
}
