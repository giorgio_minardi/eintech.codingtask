﻿using System;

namespace Eintech.CodingTask.Web.Data.Entities
{
    public abstract class AuditedEntity : BaseEntity
    {
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

    }
}
