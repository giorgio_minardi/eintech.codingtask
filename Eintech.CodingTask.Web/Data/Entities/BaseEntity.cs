﻿using System.ComponentModel.DataAnnotations;

namespace Eintech.CodingTask.Web.Data.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
