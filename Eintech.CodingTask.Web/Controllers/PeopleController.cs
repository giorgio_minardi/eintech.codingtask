﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eintech.CodingTask.Web.Data.EF;
using Eintech.CodingTask.Web.Data.Entities;
using Eintech.CodingTask.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Eintech.CodingTask.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly ICodingTaskDataContext _dataContext;

        public PeopleController(ICodingTaskDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        // GET: api/<PeopleController>
        [HttpGet]
        public async Task<IEnumerable<Person>> Get()
        {
            return await _dataContext.Persons.ToListAsync();
        }

        // POST api/<PeopleController>
        [HttpPost]
        public async Task Post(PersonViewModel model)
        {
                var person = new Person() { Name = model.Name };
                await _dataContext.Persons.AddAsync(person);
                await _dataContext.SaveChangesAsync();
        }
   

        // DELETE api/<PeopleController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            var person = await _dataContext.Persons.FirstOrDefaultAsync(x => x.Id == id);
            if(person != null)
            {
                _dataContext.Persons.Remove(person);
                await _dataContext.SaveChangesAsync();
            }
        }
    }
}
